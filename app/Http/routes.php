<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/me', function () {
    return "Меня зовут Артём, и я белый гетеросексуальный мужчина";
});


Route::get('/location', function () {
    return "Я живу с самой замечательной стране на свете, в городе Омске!";
});

Route::get('/json/me', function () {
    return response()->json(['name' => 'Артём', 'about' => 'Белый гетеросексуальный мужчина']);
});

Route::get('/json/location', function () {
    return response()->json(['country' => 'Россия', 'city' => 'Omsk']);
});

Route::get('/hello', function () {
    return "Hello!";
});

Route::get('/hello/{name}', function ($name) {
    return "Hello, ".$name."!";
});